package polynomes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestMonome {

    @Test
    public void toStringFirst() {
        final var monome = new Monome(4, 0);
        assertEquals(monome.toString(), "+4");
    }

    @Test
    public void toStringSecond() {
        final var monome = new Monome(4, 1);
        assertEquals(monome.toString(), "+4x");
    }

    @Test
    public void monomeNeg() {
        assertThrows(DegreNegatifException.class, () -> new Monome(4, -1));
    }

}
