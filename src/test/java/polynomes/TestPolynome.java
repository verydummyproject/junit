package polynomes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPolynome {

    @Test
    public void ajoutMonomeDegreNonExistant() {
        final var p = new Polynome();
        p.ajoutMonome(new Monome(4, 2));
        p.ajoutMonome(new Monome(2, 1));
        p.ajoutMonome(new Monome(2, 0));
        assertEquals(p.toString(), "+2+2x+4x^2");
    }

    @Test
    public void ajoutMonomeDegreExistant() {
        final var p = new Polynome();
        p.ajoutMonome(new Monome(4, 2));
        p.ajoutMonome(new Monome(2, 1));
        p.ajoutMonome(new Monome(2, 0));
        p.ajoutMonome(new Monome(5, 2));
        assertEquals(p.toString(), "+2+2x+9x^2");
    }

    @Test
    public void produitPolynome() {
        final var p1 = new Polynome();
        p1.ajoutMonome(new Monome(4, 2));
        p1.ajoutMonome(new Monome(2, 1));
        p1.ajoutMonome(new Monome(2, 0));


        final var p2 = new Polynome();
        p2.ajoutMonome(new Monome(4, 2));
        p2.ajoutMonome(new Monome(2, 1));
        p2.ajoutMonome(new Monome(2, 0));

        assertEquals(p1.produit(p2).toString(), "+4+8x+20x^2+16x^3+16x^4");
        assertEquals(p2.produit(p1).toString(), "+4+8x+20x^2+16x^3+16x^4");
    }

    @Test
    public void produitPolynomeMonome() {
        final var p1 = new Polynome();
        p1.ajoutMonome(new Monome(4, 2));
        p1.ajoutMonome(new Monome(2, 1));
        p1.ajoutMonome(new Monome(2, 0));

        assertEquals(p1.produit(new Monome(1, 1)).toString(), "+2x+2x^2+4x^3");
    }

}
